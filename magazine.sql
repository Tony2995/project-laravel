-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 07 2016 г., 06:33
-- Версия сервера: 5.1.62-community
-- Версия PHP: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `magazine`
--

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_05_094648_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `published_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `excerpt`, `content`, `published_at`, `published`, `created_at`, `updated_at`, `author_id`) VALUES
(9, 'Используем BEM', 'ispolzuem-bem', 'БЭМ (Блок-Элемент-Модификатор) — методология web-разработки, а также набор интерфейсных библиотек, фреймворков и вспомогательных инструментов.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolor eaque eius eum ex illo iste maiores non obcaecati officiis pariatur perferendis reiciendis repellat tenetur, ut. Distinctio excepturi nobis odit quibusdam tenetur? Architecto aut cupiditate doloremque dolorum eveniet ipsa labore libero natus neque perspiciatis porro, repellat sapiente sequi tempore ullam?', '2016-06-06 08:06:55', 1, '2016-06-06 00:06:55', '2016-06-06 00:06:55', NULL),
(10, 'Что такое DOM', 'chto-takoe-dom', 'DOM (от англ. Document Object Model — «объектная модель документа») — это не зависящий от платформы и языка программный интерфейс, позволяющий программам и скриптам получить доступ к содержимому HTML, XHTML и XML-документов, а также изменять содержимое, структуру и оформление таких документов.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolor eaque eius eum ex illo iste maiores non obcaecati officiis pariatur perferendis reiciendis repellat tenetur, ut. Distinctio excepturi nobis odit quibusdam tenetur? Architecto aut cupiditate doloremque dolorum eveniet ipsa labore libero natus neque perspiciatis porro, repellat sapiente sequi tempore ullam?', '2016-06-06 08:06:55', 0, '2016-06-06 00:06:55', '2016-06-06 00:06:55', NULL),
(11, 'React.js', 'react-js', 'React.js — фреймворк для создания интерфейсов от Facebook. В последнее время онлайн и на MoscowJS было много разговоров о нём. Как всегда с опозданием, я смог опробовать React только на днях. В этой статье я в двух словах расскажу о концепциях, которые стоят за фреймворком, и покажу, как быстро начать с ним работать.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolor eaque eius eum ex illo iste maiores non obcaecati officiis pariatur perferendis reiciendis repellat tenetur, ut. Distinctio excepturi nobis odit quibusdam tenetur? Architecto aut cupiditate doloremque dolorum eveniet ipsa labore libero natus neque perspiciatis porro, repellat sapiente sequi tempore ullam?', '2016-06-06 08:06:55', 0, '2016-06-06 00:06:55', '2016-06-06 00:06:55', NULL),
(12, NULL, '', NULL, NULL, NULL, 0, '2016-06-06 08:55:08', '2016-06-06 08:55:08', NULL),
(13, 'gans', 'gans', 'gans', 'gans', '2016-09-15 16:00:00', 0, '2016-06-06 09:02:05', '2016-06-06 09:02:05', NULL),
(14, 'home-posts', 'home-posts', 'home-postshome-postshome-posts', 'home-postshome-postshome-postshome-postshome-postshome-postshome-postshome-postshome-postshome-postshome-postshome-postshome-posts', '2016-06-05 16:00:00', 0, '2016-06-06 09:26:21', '2016-06-06 09:26:21', NULL),
(15, 'qwerty', 'qwerty', 'qwerty', 'qwertyqwertyqwerty', '2016-06-05 16:00:00', 0, '2016-06-06 09:47:32', '2016-06-06 09:47:32', 1),
(16, 'gerhstb', 'gtge', 'aehbt', 'aerh', '2016-06-05 16:00:00', 0, '2016-06-06 09:49:20', '2016-06-06 09:49:20', 1),
(17, 'Статья Тони', 'bobik', 'Здесь будет статья Тони', 'Вот моя статья :)', '2016-06-05 16:00:00', 0, '2016-06-06 10:02:43', '2016-06-06 10:02:43', 1),
(18, 'TYUI', 'rtyu', 'text text text', 'text text texttext text texttext text texttext text text', '2016-06-05 16:00:00', 1, '2016-06-06 11:15:05', '2016-06-06 11:15:05', 1),
(19, 'url-statia', 'url-statia', 'url-statiaurl-statiaurl-statiaurl-statia', 'loremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremloremlorem', '2016-06-06 16:00:00', 1, '2016-06-06 17:40:35', '2016-06-06 17:40:35', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Tony', 'anton2995@yandex.ru', '$2y$10$EUpk9LthILm36XifrffGS.gW1YS3k4BU1XtxJxKn.DNp.BkUqMKQa', 'HO9KSCqe7TjFaBDXxwayHP4Tvk6Rv3fEMGuEKySwEtkS9czCgYLT2fzdaaeX', '2016-06-05 05:29:10', '2016-06-06 18:00:57', 2),
(2, 'Bob', 'bob@gmail.com', '$2y$10$744E1OwedMj8Pe.7Bjgxe.gYpcIr1EahlMQwJcOU/w8JTPd7lEpK2', 'tzhCH8RdXWwi6qmFi1n2uIOpGOR2ctGZbH47AIxK2ZPce4fyVTofDur6ezdg', '2016-06-05 19:49:48', '2016-06-05 20:07:43', 2),
(3, 'admin', 'admin@admin.ru', '$2y$10$Dz73s3gtPqVhSU9Smk5BhOuxSa9Gos/j7ImqXBGdyRC1PAUMbyDYq', 'f8LGyJGT8EzlwaSNFUEmZNyGcI3woQbCH4b5c1n9l8p05POXkgHD42s04Xbq', '2016-06-05 20:09:11', '2016-06-06 11:16:21', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
