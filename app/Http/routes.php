<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'HomeMagazine@index');


	Route::get('home', 'HomeController@index');
	Route::get('posts/{slug}', 'HomeMagazine@post');
	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

	get('/', ['as' => 'main', 'uses' => 'HomeMagazine@index']);
	get('unpublished', ['as' => 'post.unpublished', 'uses' => 'HomeMagazine@unpublished']);
	get('all', ['as' => 'post.all', 'uses' => 'HomeMagazine@allPosts']);

//get('{slug}', ['as' => 'post.slug', 'uses' => 'HomeMagazine@getPost']);

	$router->resource('post', 'HomeMagazine');

	//get('post/create', ['as' => 'post.create', 'uses' => 'HomeMagazine@create']);
	//post('post', ['as' => 'post.store', 'uses' => 'HomeMagazine@store']);
	get('myposts', ['as' => 'mypost', 'uses' => 'HomeMagazine@myPosts']);
	//post('delete', ['as' => 'post.delete', 'uses' => 'HomeMagazine@deletePost']);

Route::get('delete{slug}', [
	'as' => 'delete', 'uses' => 'HomeMagazine@getDelete'
]);
	
