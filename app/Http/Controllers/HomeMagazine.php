<?php namespace App\Http\Controllers;

use Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;


class HomeMagazine extends Controller {

	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Post $postModel)
	{
		//$posts = Post::all(); //получить все
		//$posts = Post::latest('id')->get(); // получить и отсортировать по id
		//$posts = Post::latest('published_at')->get(); // получить и отсротировать по дате публикации
		//$posts = Post::latest('published_at') // проверка публикации
		//	->where('published_at', '<=', Carbon::now())// если публикация меньше или равна нынешней дате то публикуем
		//	->get(); // посты на будущее не публикуем, ждем...

		//dd(Auth::user()->id);
		$posts = $postModel->getPublishedPosts();
		if (Auth::guest()) {
			$status = 0;
		}
		else {
			$status = Auth::user()->status;
		}

		return view('homemagazine.index', ['posts' => $posts, 'status' => $status]);
	}

	public function unpublished(Post $postModel) {
		if (Auth::guest()) {
			$status = 0;
		}
		else {
			$status = Auth::user()->status;
		}


		if ($status === 1) {
			$posts = $postModel->getUnPublishedPosts();
			return view('homemagazine.index', ['posts' => $posts, 'status' => $status]);
		}
		else {
			return view('errors.404');
		}
	}
	
	public function allPosts(Post $postModel) {
		if (Auth::guest()) {
			$status = 0;
		}
		else {
			$status = Auth::user()->status;
		}
		
		if ($status === 1) {
			$posts = $postModel->getAllPosts();
			return view('homemagazine.index', ['posts' => $posts, 'status' => $status]);
		}
		else {
			return view('errors.404');
		}

	}


	public function post($slug) {
		$users = User::all();
		$posts = DB::select('SELECT * FROM `posts` WHERE `slug` = "'.$slug.'"');

		return view('homemagazine.reed', ['posts' => $posts], ['users' => $users]);
	}
	
	
	public function myPosts(Post $postModel) {

		if (Auth::guest()) {
			return view('errors.404');
		}
		else {
			$posts = $postModel->getAuthorPosts();
			return view('homemagazine.myposts', ['posts' => $posts]);
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('post.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Post $postModel, Request $request)
	{
		//dd($request->all());
		$postModel->create($request->all());

		return redirect('myposts');
	}

	public function getDelete() {
		dd(Route::getFacadeRoot('{slug}'));
		//dd(DB::table('posts')->get());
		return redirect('myposts');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

}
