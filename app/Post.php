<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Eloquent;
use Illuminate\Support\Facades\Auth;


class Post extends Model {

    protected $guarded = array();

	public function getPublishedPosts() {

        $posts = $this->latest('published_at')->published()->get();
        return $posts;
    }

    public function getUnPublishedPosts() {
        $posts = $this->latest('published_at')->unPublished()->get();

        return $posts;
    }
    
    public function getAllPosts() {
        $posts = Post::all();

        return $posts;
    }

    public function getPostId() {
        $posts = $this->latest('published_at')->GetPostId()->get();

        return $posts;
    }
    
    public function getAuthorPosts() {
        $posts = $this->latest('published_at')->AuthorPosts()->get();
        return $posts;
    }

    public function scopeAuthorPosts($query) {
        
        $query->where('author_id', '=', Auth::user()->id);
    
    }
    
    

    public function scopePublished($query) {
        $query->where('published', '=', 1);

    }

    public function scopeUnPublished($query) {
        $query->where('published_at', '=>', Carbon::now())
            ->orWhere('published', '=', 0);
    }
    
}





