@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1>Создать статью</h1>

                {!! Form::open(array('url' => '/post')) !!}
                {!! Form::token() !!}


                @include('post._form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop