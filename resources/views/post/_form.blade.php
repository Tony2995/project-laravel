
<div class="form-group">
    {!! Form::label('url статьи') !!}
    {!! Form::text( 'slug', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('Заголовок') !!}
    {!! Form::text( 'title', null, ['class' => 'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Текст презентации') !!}
    {!! Form::textarea( 'excerpt', null, ['class' => 'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Текст статьи') !!}
    {!! Form::textarea( 'content', null, ['class' => 'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Опубликовать') !!}
    {!! Form::checkbox( 'published', 1, false) !!}
</div>
<div class="form-group">
    {!! Form::label('Опубликовать: ') !!}
    {!! Form::input( 'date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
</div>
    <input type="hidden" name="author_id" value="{{ Auth::user()->id }}">

<div class="form-group">
    {!! Form::submit( 'Создать', ['class' => 'btn btn-primary']) !!}
</div>




