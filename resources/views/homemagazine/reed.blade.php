@extends('app')

@section('content')
    <div class="container">
        @foreach($posts as $post)
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $post->title }}</h2>
            </div>
        </div>
        {{ $post->content }}
            <div class="page-header"></div>
        @endforeach
    </div>
    @if (Auth::user())
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-default">{!! link_to_route('delete', 'Удалить статью') !!}</button>
            </div>
        </div>
    </div>
    @endif
@stop
