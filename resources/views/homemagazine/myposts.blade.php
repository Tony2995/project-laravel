@extends('app')
@section('content')
@foreach($posts as $post)
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading"><h1>{{ $post->title }}</h1></div>
            <div class="panel-body">
                <p>{{ $post->excerpt }}</p>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-3"><a class="btn btn-primary btn-lg" href="{{ 'posts/'.$post->slug }}" role="button">Читать
                            статью</a></div>
                    <div class="col-md-2 pull-right">Опубликованно {{ $post->published_at }}</div>
                </div>
            </div>
        </div>
    </div>
@endforeach
@stop