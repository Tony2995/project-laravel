@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header text-center">IT Magazine</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h1>{{ $post->title }}</h1></div>
                        <div class="panel-body">
                            <p>{{ $post->excerpt }}</p>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-3"><a class="btn btn-primary btn-lg" href="{{ 'posts/'.$post->slug }}" role="button">Читать
                                        статью</a></div>
                                <div class="col-md-2 pull-right">Опубликованно {{ $post->published_at }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if ($status === 2 or $status === 1)
                <div class="btn-group btn-group-justified page-header" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">{!! link_to_route('main', 'опубликованно') !!}</button>
                    </div>
                    @if($status === 1)
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default">{!! link_to_route('post.unpublished', 'неопубликованно') !!}</button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default">{!! link_to_route('post.all', 'все статьи') !!}</button>
                        </div>
                    @endif
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">{!! link_to_route('post.create', 'создать') !!}</button>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@stop
